﻿using System;
using RandomizeGame.MenuHandling;

namespace RandomizeGame
{
	public class Program
	{
		static void Main(string[] args)
		{
			string end = "";
			do
			{
				ConsoleKey choice = MenuDisplay.MenuDisplayHandling();

				HandlingOfMenu.BehaviorOfChoiceMenu(choice);

			} while (end != "Dupa");
			
			Console.WriteLine("Koniec gry - wciśnij przycisk");
			Console.ReadLine();
		}
	}
}
