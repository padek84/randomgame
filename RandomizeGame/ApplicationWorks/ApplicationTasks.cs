﻿using System;
using System.Collections.Generic;
using RandomizeGame.Enums;

namespace RandomizeGame.ApplicationWorks
{
	public class ApplicationTasks
	{
		public static ConsoleKey UpOrDownButton(int currentNumber)
		{
			ConsoleKey currentKey;

			do
			{
				Console.Clear();
				Console.WriteLine($"Is the next number will be higher or lower than {currentNumber} ?");
				Console.WriteLine("Please select 'up arrow' or 'down arrow'");

				currentKey = Console.ReadKey().Key;

			} while (currentKey != ConsoleKey.UpArrow && currentKey != ConsoleKey.DownArrow);

			return currentKey;
		}

		public static List<int> ReturnRandomizeNumber(int min, int max, List<int> otherNumbersList)
		{
			ProcessStatus result = ProcessStatus.Unknown;

			do
			{
				int currentNumber = new Random().Next(min, max);

				if (otherNumbersList == null)
				{
					otherNumbersList = new List<int> { currentNumber };

					result = ProcessStatus.Success;
				}
				else
				{
					if (!otherNumbersList.Contains(currentNumber))
					{
						otherNumbersList.Add(currentNumber);
						result = ProcessStatus.Success;
					}
				}

			} while (result == ProcessStatus.Unknown);

			return otherNumbersList;
		}

		public static ProcessStatus DownArrowBehaviour(ConsoleKey userKey, List<int> newNumbers, ProcessStatus result)
		{
			var length = newNumbers.Count;

			if (newNumbers[length - 1] < newNumbers[length - 2])
			{
				Console.WriteLine($"Poprzednia: {newNumbers[length - 2]} i jest mniejsza niż aktualna: {newNumbers[length - 1]}");
				Console.WriteLine($"Wybrałeś {userKey} i trafiłeś");

				result = ProcessStatus.Success;

				return result;
			}

			Console.WriteLine($"Poprzednia: {newNumbers[length - 2]} i jest mniejsza niż aktualna: {newNumbers[length - 1]}");
			Console.WriteLine($"Wybrałeś {userKey} i niestety nie trafiłeś");

			result = ProcessStatus.Failed;

			return result;
		}

		public static ProcessStatus UpArrowBehaviour(ConsoleKey userKey, List<int> newNumbers, ProcessStatus result)
		{
			var length = newNumbers.Count;
			if (newNumbers[length - 1] > newNumbers[length - 2])
			{
				Console.WriteLine($"Poprzednia: {newNumbers[length - 2]} jest większa niż aktualna: {newNumbers[length - 1]}");
				Console.WriteLine($"Wybrałeś {userKey} i trafiłeś");

				result = ProcessStatus.Success;

				return result;
			}

			Console.WriteLine($"Poprzednia: {newNumbers[length - 2]} jest większa niż aktualna: {newNumbers[length - 1]}");
			Console.WriteLine($"Wybrałeś {userKey} i niestety nie trafiłeś");

			result = ProcessStatus.Failed;

			return result;
		}

		public static ProcessStatus MainGameWorkflow()
		{
			ProcessStatus result = ProcessStatus.Unknown;

			List<int> otherNumberList = null;

			int i = 0;
			do
			{
				i++;
				List<int> newNumbers;

				if (i == 1)
				{
					newNumbers = ReturnRandomizeNumber(1, 11, otherNumberList);
				}

				else
				{
					int currentNr = otherNumberList[otherNumberList.Count - 1];

					ConsoleKey userKey = UpOrDownButton(currentNr);

					newNumbers = ReturnRandomizeNumber(1, 11, otherNumberList);

					if (userKey == ConsoleKey.DownArrow)
					{
						result = DownArrowBehaviour(userKey, newNumbers, result);
					}

					if (userKey == ConsoleKey.UpArrow)
					{
						result = UpArrowBehaviour(userKey, newNumbers, result);
					}

					if (result == ProcessStatus.Failed)
					{
						Console.WriteLine("Niestety - przegrałeś!!!");
						Console.WriteLine("Wcisnij przycisk, aby zakończyć grę");
						Console.ReadKey();

						break;
					}

					Console.WriteLine("Wcisnij przycisk, aby pojsc dalej");
					Console.ReadKey();
				}

				otherNumberList = newNumbers;

			} while (i <= 5 || result != ProcessStatus.Failed);

			return result;
		}
	}
}
