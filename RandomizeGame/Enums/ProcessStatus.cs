﻿namespace RandomizeGame.Enums
{
	public enum ProcessStatus
	{
		Failed = 0,
		Success = 1,
		Unknown = 2
	}
}
