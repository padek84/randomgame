﻿using System;

namespace RandomizeGame.MenuHandling
{
	public class MenuDisplay
	{
		public static ConsoleKey MenuDisplayHandling()
		{
			ConsoleKey keySelected;

			do
			{
				Console.Clear();

				Console.WriteLine("What would you like to do?:");
				Console.WriteLine("1. Start the game");
				Console.WriteLine("2. Check the last result - NOT WORK");
				Console.WriteLine("3. Check the statistics - NOT WORK");
				Console.WriteLine("ESCAPE - EXIT");
				Console.WriteLine("");
				Console.WriteLine("Please tell me what you would like to do...");

				keySelected = Console.ReadKey().Key;

			} while (keySelected != ConsoleKey.NumPad1 && keySelected != ConsoleKey.D1 && keySelected != ConsoleKey.NumPad2 && keySelected != ConsoleKey.D2 && keySelected != ConsoleKey.NumPad3 && keySelected != ConsoleKey.D3 && keySelected != ConsoleKey.Escape);

			return keySelected;
		}
	}
}
