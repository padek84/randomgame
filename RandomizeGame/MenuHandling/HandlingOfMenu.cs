﻿using System;
using RandomizeGame.ApplicationWorks;

namespace RandomizeGame.MenuHandling
{
	public class HandlingOfMenu
	{
		public static string BehaviorOfChoiceMenu(ConsoleKey choice)
		{
			switch (choice)
			{
				case ConsoleKey.NumPad1:
				case ConsoleKey.D1:
				{
					ApplicationTasks.MainGameWorkflow();
					return null;
				}
					
				case ConsoleKey.NumPad2:
				case ConsoleKey.D2:
					{
					return null;
				}

				case ConsoleKey.NumPad3:
				case ConsoleKey.D3:
					{
					return null;
				}

				case ConsoleKey.Escape:
				{
					Environment.Exit(0);
					return null;
				}

				default:
					return null;
			}
		}
	}
}
